package global;

public class HeaderConstants {
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";

    public static final String GET_ONLINE_FRIENDS = "getOnlineFriends";
    public static final String LOGIN_OK = "loginOK";
    public static final String LOGIN_FAILED = "loginFailed";

    public static final String UPDATE_ONLINE_FR = "updateOnlineFriend";
    public static final String LOAD_PROFILE_IMAGE = "loadProfileImage";
    public static final String LOAD_FRIENDS = "loadFriends";
    public static final String GET_CHAT_LOG = "getChatLog";
    public static final String CHAT_LOG = "chatLog";
    public static final String CHAT = "chat";
    public static final String GET_AVT_CHAT = "getAvtChat";
    public static final String UPDATE_ONLINE_FRIEND = "updateOnlineFriend";

}
