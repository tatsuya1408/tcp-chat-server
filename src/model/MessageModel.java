package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MessageModel extends DataModel implements Serializable {

    private List<String> messageList = new ArrayList<>();

    public MessageModel() {

    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }
}
