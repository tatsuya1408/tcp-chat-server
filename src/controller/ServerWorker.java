package controller;

import dao.AccountDAO;
import dao.AccountDAOImpl;
import dao.MessageDAO;
import dao.MessageDAOImpl;
import global.HeaderConstants;
import model.Account;
import model.DataModel;
import model.ImageModel;
import model.MessageModel;
import view.View;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tatsuya
 */
public class ServerWorker implements Runnable {
    private View view;
    private AccountDAO accountDAO;

    private ServerController masterServer;
    private Socket clientSocket;
    private ObjectInputStream readFromClient;
    private ObjectOutputStream writeToClient;

    private MessageDAO messageDAO = new MessageDAOImpl();

    public ServerWorker(ServerController masterServer, Socket clientSocket) {
        this.view = new View();
        this.accountDAO = new AccountDAOImpl();
        this.masterServer = masterServer;
        this.clientSocket = clientSocket;
    }

    private DataModel readData() throws IOException, ClassNotFoundException {
        return (DataModel) this.readFromClient.readObject();
    }

    public void sendData(DataModel dataModel) throws IOException {
        this.writeToClient.writeObject(dataModel);
    }

    private boolean performLogin(Account account) {
        try {
            return accountDAO.checkValidation(account);
        } catch (SQLException e) {
            return false;
        }
    }

    private List<Account> findOnlineFriends(Account account) {
        List<Account> listAllFriends = null;
        try {
            listAllFriends = accountDAO.getAllFriends(account);
            System.out.println("size of list friends is "+listAllFriends.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Account> list = new ArrayList<>();
        for (Account u : this.masterServer.getWorkerMap().keySet()) {
            if (listAllFriends.contains(u)) {
                list.add(u);
            }
        }
        System.out.println("size of online friends is "+list.size());
        return list;
    }

    @Override
    public void run() {
        try {
            this.writeToClient = new ObjectOutputStream(this.clientSocket.getOutputStream());
            this.readFromClient = new ObjectInputStream(this.clientSocket.getInputStream());

            while (true) { // listening to client....
                DataModel dataModel = readData();
                if (dataModel != null) {
                    String status = dataModel.getStatus();
                    view.log("a new request found. status = " + status);
                    switch (status) {
                        case HeaderConstants.LOGIN: {

                            /* validate account and send notification to client.
                            if account is valid,
                            notify all his/her friends that he/she is online now
                            */
                            Account sendAccount = dataModel.getSendAccount();
                            boolean valid = performLogin(sendAccount);

                            if (valid) {
                                this.masterServer.getWorkerMap().put(dataModel.getLoggedAccount(), this);
                                dataModel.setStatus(HeaderConstants.LOGIN_OK);
                            } else {
                                dataModel.setStatus(HeaderConstants.LOGIN_FAILED);
                            }

                            sendData(dataModel);// notify to client

                            this.masterServer.notifyAllWorkers(
                                    HeaderConstants.UPDATE_ONLINE_FR,
                                    dataModel.getSendAccount()
                            );
                            break;
                        }

                        case HeaderConstants.LOAD_PROFILE_IMAGE: {
                            String user = dataModel.getMessageContent();
                            Image i;
                            try {
                                i = ImageIO.read(new File("avt/" + user + ".png"));
                            } catch (IOException ex) {
                                i = ImageIO.read(new File("avt/default.png"));
                            }
                            ImageModel imageModel = new ImageModel();
                            imageModel.setStatus(HeaderConstants.LOAD_PROFILE_IMAGE);
                            imageModel.setReceiveAccount(dataModel.getReceiveAccount());
                            imageModel.setImageIcon(new ImageIcon(i));
                            sendData(imageModel);
                            break;
                        }

                        case HeaderConstants.LOAD_FRIENDS: {
                            DataModel dataModelFr = new DataModel(HeaderConstants.LOAD_FRIENDS);
                            dataModelFr.setOnlineFriends(findOnlineFriends((dataModel.getLoggedAccount())));
                            sendData(dataModelFr);
                            break;
                        }

                        case HeaderConstants.GET_CHAT_LOG: {
                            MessageModel messageModel = new MessageModel();
                            messageModel.setStatus(HeaderConstants.CHAT_LOG);
                            messageModel.setSendAccount(dataModel.getSendAccount());
                            messageModel.setReceiveAccount(dataModel.getReceiveAccount());

                            List<String> logContent = findConversation(dataModel);
                            messageModel.setMessageList(logContent);
                            sendData(messageModel);
                            break;
                        }

                        case HeaderConstants.CHAT: {
                            this.masterServer.findWorker(dataModel);
                            messageDAO.addMessage(dataModel);
                            break;
                        }

                    }
                }

            }
        } catch (IOException | ClassNotFoundException e) {
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean performRegistration(Account account) {
        try {
            return accountDAO.addAccount(account);
        } catch (SQLException e) {
            return false;
        }
    }

    private List<String> findConversation(DataModel dataModel) {
        try {
            return messageDAO.getChatLogByList(dataModel);
        } catch (SQLException e) {
            return null;
        }
    }
}
