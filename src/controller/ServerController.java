package controller;

import dao.AccountDAO;
import dao.AccountDAOImpl;
import model.Account;
import model.DataModel;
import view.View;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * main thread - master thread
 * controls all sub-threads (slave threads) ServerWorker
 * each ServerWorker is an instance of a client
 *
 * @author tatsuya
 */
public class ServerController implements Runnable {
    private View view;
    private AccountDAO accountDAO;

    /**
     * all online account in system
     * key: account, represented for client
     * value: thread which communicates with that client
     */
    private Map<Account, ServerWorker> workerMap = new HashMap<>();

    public ServerController() {
        this.view = new View();
        this.accountDAO = new AccountDAOImpl();
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(1234);
            view.log("Listening on port 1234....");
            while (true) {
                Socket socket = serverSocket.accept();
                view.log("Accepted a client....");
                new Thread(new ServerWorker(this, socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * send notifications to all online friends of an account
     *
     * @param status  status of account you want to notify to your friends
     *                example: login, logout, updateOnline,...
     * @param account logged account
     */
    public void notifyAllWorkers(String status, Account account) {

        AccountDAO accountDAO = new AccountDAOImpl();
        List<Account> listAllFriends = null;
        try {
            listAllFriends = accountDAO.getAllFriends(account); // get all friends of account from DB

            for (Account u : this.workerMap.keySet()) {
                if (listAllFriends.contains(u)) { // if account's friend is online
                    DataModel dataModel = new DataModel(status);
                    dataModel.setLoggedAccount(account);
                    //do your logic code here
                    try {
                        this.workerMap.get(u).sendData(dataModel);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SQLException e) {
            view.log("an error occurred when getting friends of account " + account.getUserName());
            e.printStackTrace();
        }

    }

    /**
     * search for a specific worker based on receivedAccount attribute in DataModel object
     * and send data to that worker
     */
    public void findWorker(DataModel model) {
        view.log("SendUser = " + model.getSendAccount().getUserName());
        view.log("ReceiveUser = " + model.getReceiveAccount().getUserName());
        view.log("messageContent = " + model.getMessageContent());
        ServerWorker worker = this.workerMap.get(model.getReceiveAccount());
        if (worker != null) {
            view.log("found worker = " + worker + " to send data");
            try {
                worker.sendData(model);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            view.log("worker not found (not online)");
        }
    }

    public Map<Account, ServerWorker> getWorkerMap() {
        return this.workerMap;
    }
}
