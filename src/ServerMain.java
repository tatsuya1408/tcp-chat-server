import controller.ServerController;

public class ServerMain {
    public static void main(String[] args) {
        Thread masterThread = new Thread(new ServerController());
        masterThread.setName("master thread");
        masterThread.start();
    }
}
