package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * a singleton class
 * following Singleton design pattern
 *
 * @author tatsuya
 */
public class MySQLConnector {
    private static Connection connection;
    private static MySQLConnector ourInstance = new MySQLConnector();

    public static MySQLConnector getInstance() {
        return ourInstance;
    }

    /**
     * get connection to MySQL RDBMS
     */
    public Connection getMySQLConnection() {
        return connection;
    }

    private MySQLConnector() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("config.properties"));
            String URL = properties.getProperty("mysql.url");
            String username = properties.getProperty("mysql.username");
            String password = properties.getProperty("mysql.password");
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, username, password);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
