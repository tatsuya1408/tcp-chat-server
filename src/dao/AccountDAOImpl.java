package dao;


import model.Account;
import utils.MySQLConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDAOImpl implements AccountDAO {

    private Connection connection;

    private MySQLConnector mySQLConnector = MySQLConnector.getInstance();

    public AccountDAOImpl() {
        this.connection = this.mySQLConnector.getMySQLConnection();
    }

    @Override
    public boolean checkValidation(Account account) throws SQLException {
        String sqlCommand = "SELECT * FROM account WHERE Username = ? AND Password = ?";
        PreparedStatement statement = connection.prepareStatement(sqlCommand);
        statement.setString(1, account.getUserName());
        statement.setString(2, account.getPassword());
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            if (res.getString(1).compareTo(account.getUserName()) == 0) return true;
        }
        return false;
    }

    @Override
    public boolean addAccount(Account account) throws SQLException {
        String sqlCommand = "INSERT INTO account VALUES (?,?)";
        PreparedStatement statement = this.connection.prepareStatement(sqlCommand);
        statement.setString(1, account.getUserName());
        statement.setString(2, account.getPassword());
        int aff = statement.executeUpdate();
        if (aff == 0) return false;
        return true;
    }

    @Override
    public List<Account> getAllFriends(Account account) throws SQLException {
        List<Account> listFriends = new ArrayList<>();
        String sqlCommand = "SELECT username2 FROM friends WHERE username1 = ?";
        PreparedStatement statement = this.connection.prepareStatement(sqlCommand);
        statement.setString(1, account.getUserName());
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Account u = new Account();
            u.setUserName(resultSet.getString(1));
            listFriends.add(u);
        }
        return listFriends;
    }

    @Override
    public List<Account> getAllNotFriends(Account account) throws SQLException {
        List<Account> listAccount = new ArrayList<>();
        String sqlCommand = "SELECT Username " +
                "FROM account " +
                "WHERE (Username NOT IN (SELECT username2 FROM friends WHERE username1 = ?) " +
                "AND Username <> ? )";
        PreparedStatement statement = this.connection.prepareStatement(sqlCommand);
        statement.setString(1, account.getUserName());
        statement.setString(2, account.getUserName());

        /* using CallableStatement with StoredProcedure
        String sqlCommand = "{CALL getAddFriends(?)}";
        CallableStatement statement = this.connection.prepareCall(sqlCommand);
        statement.setString(1, account.getUserName());
        */
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Account acc = new Account();
            acc.setUserName(resultSet.getString(1));
            listAccount.add(acc);
        }
        return listAccount;
    }
}