package dao;



import model.DataModel;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface MessageDAO {
    public Map<String, String> getChatLogByMap(DataModel messageModel) throws SQLException;
    public void addMessage(DataModel dataModel) throws SQLException;
    public List<String> getChatLogByList(DataModel dataModel) throws SQLException;
}

