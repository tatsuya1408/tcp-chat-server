package dao;


import model.DataModel;
import utils.MySQLConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MessageDAOImpl implements MessageDAO {
    private Connection connection;
    private MySQLConnector mySQLConnector = MySQLConnector.getInstance();

    public MessageDAOImpl() {
        this.connection = this.mySQLConnector.getMySQLConnection();
    }

    public Date getUtilDate(java.sql.Timestamp sqlTimestampDate) {
        return new Date(sqlTimestampDate.getTime());
    }

    @Override
    public Map<String, String> getChatLogByMap(DataModel messageModel) throws SQLException {
        Map<String, String> chatLog = new HashMap<>();
        System.out.println("send: " + messageModel.getSendAccount());
        System.out.println("rev: " + messageModel.getReceiveAccount());
        String sqlCommand = "SELECT * FROM message WHERE acc1 = ? OR acc1 = ? ORDER BY time";
        PreparedStatement statement = this.connection.prepareStatement(sqlCommand);
        statement.setString(1, messageModel.getSendAccount().getUserName());
        statement.setString(2, messageModel.getReceiveAccount().getUserName());
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String key = getKey(resultSet.getString(1) + "|",
                    resultSet.getString(2) + "|",
                    getUtilDate(resultSet.getTimestamp(3)));
            String value = resultSet.getString(4);
            chatLog.put(key, value);
        }
        return chatLog;
    }

    @Override
    public List<String> getChatLogByList(DataModel messageModel) throws SQLException {
        List<String> chatLog = new ArrayList<>();
        System.out.println("send: " + messageModel.getSendAccount());
        System.out.println("rev: " + messageModel.getReceiveAccount());
        String sqlCommand = "SELECT * FROM message " +
                "WHERE (acc1 = ? AND acc2 = ?) " +
                "OR (acc1 = ? AND acc2 = ?) " +
                "ORDER BY time;";
        PreparedStatement statement = this.connection.prepareStatement(sqlCommand);
        statement.setString(1, messageModel.getSendAccount().getUserName());
        statement.setString(2, messageModel.getReceiveAccount().getUserName());
        statement.setString(3, messageModel.getReceiveAccount().getUserName());
        statement.setString(4, messageModel.getSendAccount().getUserName());
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String key =
                    resultSet.getString(1) + "|" +
                            resultSet.getString(2) + "|" +
                            getUtilDate(resultSet.getTimestamp(3)).toString() + "|" +
                            resultSet.getString(4);
            chatLog.add(key);
        }
        return chatLog;
    }

    @Override
    public void addMessage(DataModel dataModel) throws SQLException {
        String sqlCommand = "INSERT INTO message VALUES(?,?,?,?)";
        PreparedStatement statement = this.connection.prepareStatement(sqlCommand);
        statement.setString(1, dataModel.getSendAccount().getUserName());
        statement.setString(2, dataModel.getReceiveAccount().getUserName());

        Date ultiDate = new Date();
        java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(ultiDate.getTime());

        statement.setTimestamp(3, sqlTimeStamp);
        statement.setString(4, dataModel.getMessageContent());
        statement.executeUpdate();
    }

    private String getKey(String string, String string1, Date string2) {
        StringBuilder s = new StringBuilder();
        s.append(string);
        s.append(string1);
        s.append(string2.toString());
        System.out.println(s);
        return s.toString();
    }
}