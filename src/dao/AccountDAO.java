package dao;


import model.Account;

import java.sql.SQLException;
import java.util.List;

/**
 * an interface which interact with Account table in database
 * following Data Access Object design pattern
 *
 * @author tatsuya
 */
public interface AccountDAO {

    /**
     * validate an account in database, perform login task
     *
     * @param account target account
     * @return true if account is validation
     */
    public boolean checkValidation(Account account) throws SQLException;

    /**
     * add an account to database
     *
     * @param account account to add
     * @return true if account is valid to add, false if account is invalid to add
     */
    public boolean addAccount(Account account) throws SQLException;

    /**
     * get all friends of an account
     *
     * @param account account in system
     * @return all friends of account
     */
    public List<Account> getAllFriends(Account account) throws SQLException;

    /**
     * get all people who aren't be friend with an account
     *
     * @param account account in system
     * @return all not-friend
     */
    public List<Account> getAllNotFriends(Account account) throws SQLException;
}
